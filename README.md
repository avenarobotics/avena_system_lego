# avena_system_lego

# avena_system_lego

lego bricks for avena system

## Getting started

Instalation process for open3D 


1. Clone github repository
`git clone --recursive https://github.com/isl-org/Open3D`

2. Install missing dependecies
`cd Open3D &&  sudo ./util/install_deps_ubuntu.sh`

3. Create folder for build 

`mkdir build && cd build`

4. Generate makefile 
`cmake -DBUILD_SHARED_LIBS=ON -DBUILD_CUDA_MODULE=ON -DBUILD_LIBREALSENSE=ON -DUSE_SYSTEM_LIBREALSENSE=ON -DCMAKE_C_COMPILER=$(which gcc-8) -DCMAKE_CXX_COMPILER=$(which g++-8) .. `

5. Build open3D
`sudo make -j$(nproc) `

6. Install 
`sudo make install`

7. add logging library for open3D

`sudo apt update && sudo apt install libfmt-dev`

<details><summary>After this step you can build tsdf example</summary>

`mkdir build && cd build`

`cmake .. && make`

**To run example you need at least one camera connected to your PC and execute**

`./Camera_test -c`

, resulting pointcloud will be saved in the build directory

</details>

<details><summary>CUDA</summary>
CUDA 11.4 is working fine (Driver Version: 470.57.02 ) I had to add gcc flags because there were some errors related to cuda packages
</details>
