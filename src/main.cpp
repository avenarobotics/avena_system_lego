// ----------------------------------------------------------------------------
// -                        Open3D: www.open3d.org                            -
// ----------------------------------------------------------------------------
// The MIT License (MIT)
//
// Copyright (c) 2018-2021 www.open3d.org
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
// ----------------------------------------------------------------------------

#include <chrono>
#include <memory>
#include <string>

#include "open3d/Open3D.h"
#include "open3d/pipelines/integration/ScalableTSDFVolume.h"
#include "open3d/core/CUDAUtils.h"
// #include <librealsense2/rs.hpp>
#include <thread>

using namespace open3d;
namespace tio = open3d::t::io;
namespace sc = std::chrono;

void PrintHelp()
{
    using namespace open3d;

    PrintOpen3DVersion();
    // clang-format off
    utility::LogInfo("Usage:");
    utility::LogInfo("    > RealSenseRecorder [-V] [-l|--list-devices] [--align] [--record rgbd_video_file.bag] [-c|--config rs-config.json]");
    utility::LogInfo(
            "Open a RealSense camera and display live color and depth streams. You can set\n"
            "frame sizes and frame rates for each stream and the depth stream can be\n"
            "optionally aligned to the color stream. NOTE: An error of 'UNKNOWN: Couldn't\n"
            "resolve requests' implies  unsupported stream format settings.");
    // clang-format on
    utility::LogInfo("");

    std::cout << "Is avaiable ? " << open3d::core::cuda::IsAvailable() << std::endl;
    std::cout << "Device count : " << open3d::core::cuda::DeviceCount() << std::endl;
}

int main(int argc, char *argv[])
{
    using namespace open3d;

    utility::SetVerbosityLevel(utility::VerbosityLevel::Debug);

    if (argc <= 1 ||
        utility::ProgramOptionExistsAny(argc, argv, {"-h", "--help"}))
    {
        PrintHelp();
        return 1;
    }

    if (utility::ProgramOptionExists(argc, argv, "--list-devices") ||
        utility::ProgramOptionExists(argc, argv, "-l"))
    {
        tio::RealSenseSensor::ListDevices();
        return 0;
    }
    if (utility::ProgramOptionExists(argc, argv, "-V"))
    {
        utility::SetVerbosityLevel(utility::VerbosityLevel::Debug);
    }
    else
    {
        utility::SetVerbosityLevel(utility::VerbosityLevel::Info);
    }
    bool align_streams = true;
    std::string config_file, bag_file;

    if (utility::ProgramOptionExists(argc, argv, "-c"))
    {
        config_file = utility::GetProgramOptionAsString(argc, argv, "-c");
    }
    else if (utility::ProgramOptionExists(argc, argv, "--config"))
    {
        config_file = utility::GetProgramOptionAsString(argc, argv, "--config");
    }
    if (utility::ProgramOptionExists(argc, argv, "--align"))
    {
        align_streams = true;
    }
    if (utility::ProgramOptionExists(argc, argv, "--record"))
    {
        bag_file = utility::GetProgramOptionAsString(argc, argv, "--record");
    }

    // Read in camera configuration.
    tio::RealSenseSensorConfig rs_cfg;
    if (!config_file.empty())
    {
        open3d::io::ReadIJsonConvertible(config_file, rs_cfg);
        for (auto option : rs_cfg.config_)
            std::cout << option.first << "---" << option.second << std::endl;
    }

    // Initialize camera.
    tio::RealSenseSensor rs1;

    if (rs1.InitSensor(rs_cfg, 0, bag_file))
    {
        std::cout << "Camera 0 is initialized" << std::endl;
    }
    else
    {
        std::cout << "Camera 0 initialization failed " << std::endl;
    }
    utility::LogInfo("{}", rs1.GetMetadata().ToString());

    // Create windows to show depth and color streams.
    bool flag_start = false, flag_record = flag_start, flag_exit = false;
    visualization::VisualizerWithKeyCallback depth_vis, color_vis, ptcld_vis;
    auto callback_exit = [&](visualization::Visualizer *vis)
    {
        flag_exit = true;
        if (flag_start)
        {
            utility::LogInfo("Recording finished.");
        }
        else
        {
            utility::LogInfo("Nothing has been recorded.");
        }
        return false;
    };
    depth_vis.RegisterKeyCallback(GLFW_KEY_ESCAPE, callback_exit);
    color_vis.RegisterKeyCallback(GLFW_KEY_ESCAPE, callback_exit);
    auto callback_toggle_record = [&](visualization::Visualizer *vis)
    {
        if (flag_record)
        {
            rs1.PauseRecord();
            utility::LogInfo(
                "Recording paused. "
                "Press [SPACE] to continue. "
                "Press [ESC] to save and exit.");
            flag_record = false;
        }
        else
        {
            rs1.ResumeRecord();
            flag_record = true;
            if (!flag_start)
            {
                utility::LogInfo(
                    "Recording started. "
                    "Press [SPACE] to pause. "
                    "Press [ESC] to save and exit.");
                flag_start = true;
            }
            else
            {
                utility::LogInfo(
                    "Recording resumed, video may be discontinuous. "
                    "Press [SPACE] to pause. "
                    "Press [ESC] to save and exit.");
            }
        }
        return false;
    };
    if (!bag_file.empty())
    {
        depth_vis.RegisterKeyCallback(GLFW_KEY_SPACE, callback_toggle_record);
        color_vis.RegisterKeyCallback(GLFW_KEY_SPACE, callback_toggle_record);
        utility::LogInfo(
            "In the visulizer window, "
            "press [SPACE] to start recording, "
            "press [ESC] to exit.");
    }
    else
    {
        utility::LogInfo("In the visulizer window, press [ESC] to exit.");
    }

    using legacyRGBDImage = open3d::t::geometry::RGBDImage;
    using legacyImage = open3d::t::geometry::Image;

    // Loop over frames from device
    legacyRGBDImage im_rgbd1;

    bool is_geometry_added = false;
    size_t frame_id = 0;

    rs1.ListDevices();
    for (auto device : rs1.EnumerateDevices())
    {
        std::cout << "--------------------------------------------------------------------------------------------------------" << std::endl;
        std::cout << device.name << std::endl;
        std::cout << device.serial << std::endl;
        for (auto config : device.valid_configs)
        {
            std::cout << config.first << "---" << std::endl;
            for (auto config_param : config.second)
                std::cout << config.first << "---" << config_param << std::endl;
        }
        std::cout << "--------------------------------------------------------------------------------------------------------" << std::endl;
    }
    rs1.StartCapture(flag_start);

    // auto volume = pipelines::integration::ScalableTSDFVolume(0.001, 0.02f, pipelines::integration::TSDFVolumeColorType::RGB8);
    std::string device_code = "CUDA:0";
    core::Device device(device_code);
    std::cout << "device.GetID : " << device.GetID() << std::endl;
    utility::LogInfo("Using device: {}", device.ToString());
    int block_count =
        utility::GetProgramOptionAsInt(argc, argv, "--block_count", 100000);
    float voxel_size = static_cast<float>(utility::GetProgramOptionAsDouble(
        argc, argv, "--voxel_size", 0.001));
    float depth_scale = static_cast<float>(utility::GetProgramOptionAsDouble(
        argc, argv, "--depth_scale", rs1.GetMetadata().depth_scale_));
    float max_depth = static_cast<float>(
        utility::GetProgramOptionAsDouble(argc, argv, "--max_depth", 1.5f));
    float sdf_trunc = static_cast<float>(utility::GetProgramOptionAsDouble(
        argc, argv, "--sdf_trunc", 0.003f));

    auto focal_length = rs1.GetMetadata().intrinsics_.GetFocalLength();
    auto principal_point = rs1.GetMetadata().intrinsics_.GetPrincipalPoint();
    open3d::core::Tensor intrinsic_t = open3d::core::Tensor::Init<double>(
        {{focal_length.first, 0, principal_point.first},
         {0, focal_length.second, principal_point.second},
         {0, 0, 1}},
        device);
    utility::LogInfo("intrinsic_t Using device: {}", intrinsic_t.GetDevice().ToString());
    std::cout << "TSD value has to be lower than " << 16 * voxel_size * 0.5 << std::endl;
    t::geometry::TSDFVoxelGrid voxel_grid({{"tsdf", core::Dtype::Float32},
                                           {"weight", core::Dtype::UInt16},
                                           {"color", core::Dtype::UInt16}},
                                          voxel_size, sdf_trunc, 16,
                                          block_count, device);
    utility::LogInfo("voxel_grid Using device: {}", voxel_grid.GetDevice().ToString());

    utility::LogInfo("voxel_grid Using device: {}", voxel_grid.GetDevice().ToString());
    std::cout << "voxel_grid block count " << voxel_grid.GetBlockCount() << std::endl;

    // auto t::
    Eigen::Matrix4d camera_extrinsics1 = Eigen::Matrix4d::Identity();
    // camera_extrinsics1.inverse();
    open3d::core::Tensor extrinsic_t = core::eigen_converter::EigenMatrixToTensor(camera_extrinsics1.inverse().eval());

    open3d::core::Tensor extrinsic_t_cuda = open3d::core::Tensor::Init<double>({{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}, device);
    utility::LogInfo("extrinsic_t_cuda Using device: {}", extrinsic_t_cuda.GetDevice().ToString());

    open3d::t::geometry::Image depth_image;
    open3d::t::geometry::Image color_image;
    do
    {
        im_rgbd1 = rs1.CaptureFrame(true, align_streams);

        utility::Timer timer_io;
        depth_image = im_rgbd1.depth_;
        color_image = im_rgbd1.color_;
        timer_io.Start();
        depth_image = depth_image.To(device);
        color_image = color_image.To(device);
        timer_io.Stop();
        utility::LogInfo("Upload  takes {}", timer_io.GetDuration());
        utility::Timer timer_bilateral;
        timer_bilateral.Start();
        depth_image = depth_image.FilterBilateral(3, 2.f, 2.f);
        timer_bilateral.Stop();
        utility::LogInfo("bilateral takes {}", timer_bilateral.GetDuration());

        utility::LogInfo("depth_image_ptr1 Using device: {}", depth_image.GetDevice().ToString());
        utility::LogInfo("depth_image_ptr1 info:  {}", depth_image.ToString());

        utility::LogInfo("color_image_ptr1 Using device: {}", color_image.GetDevice().ToString());
        utility::LogInfo("color_image_ptr1  info{}", color_image.ToString());
        std::cout << " integration.started()" << std::endl;
        utility::Timer integration;
        integration.Start();
        voxel_grid.Integrate(depth_image, color_image, intrinsic_t, extrinsic_t, rs1.GetMetadata().depth_scale_, max_depth);
        integration.Stop();
        std::cout << " integration.GetDuration();" + std::to_string(integration.GetDuration()) << std::endl;
        if (frame_id++ % 100 == 0)
        {
            using MaskCode = t::geometry::TSDFVoxelGrid::SurfaceMaskCode;

            utility::Timer ray_timer;

            ray_timer.Start();
            // for generating depth from camera position
            auto result = voxel_grid.RayCast(intrinsic_t, extrinsic_t_cuda, depth_image.GetCols(), depth_image.GetRows(), rs1.GetMetadata().depth_scale_, 0.1f, max_depth, 3.0f, MaskCode::DepthMap | MaskCode::ColorMap);
            //    checking if 4k can be generated
            // auto result = voxel_grid.RayCast(intrinsic_t, extrinsic_t_cuda, 3840, 2160, rs1.GetMetadata().depth_scale_, 0.1f, max_depth, 3.0f, MaskCode::DepthMap | MaskCode::ColorMap);

            // auto result = voxel_grid.RayCast(
            //     intrinsic_t, extrinsic_t, depth.GetCols(), depth.GetRows(),
            //     depth_scale, 0.1, depth_max, std::min(i * 1.0f, 3.0f),
            //     MaskCode::DepthMap | MaskCode::ColorMap);
            ray_timer.Stop();

            utility::LogInfo(" Raycast takes {}",
                             ray_timer.GetDuration());

            if (true)
            {
                core::Tensor range_map = result[MaskCode::RangeMap];
                t::geometry::Image im_near(
                    range_map.Slice(2, 0, 1).Contiguous() / max_depth);
                visualization::DrawGeometries(
                    {std::make_shared<open3d::geometry::Image>(
                        im_near.ToLegacy())});
                t::geometry::Image im_far(
                    range_map.Slice(2, 1, 2).Contiguous() / max_depth);
                visualization::DrawGeometries(
                    {std::make_shared<open3d::geometry::Image>(
                        im_far.ToLegacy())});
                t::geometry::Image depth_raycast(result[MaskCode::DepthMap]);
                visualization::DrawGeometries(
                    {std::make_shared<open3d::geometry::Image>(
                        depth_raycast
                            .ColorizeDepth(rs1.GetMetadata().depth_scale_, 0.1,
                                           max_depth)
                            .ToLegacy())});
                t::geometry::Image color_raycast(result[MaskCode::ColorMap]);
                visualization::DrawGeometries(
                    {std::make_shared<open3d::geometry::Image>(
                        color_raycast.ToLegacy())});
            }

            auto extracted_pointcloud = voxel_grid.ExtractSurfacePoints();
            utility::Timer integration_shadow;
            std::cout << " integration_shadow.started()" << std::endl;
            // integration_shadow.Start();
            // extracted_pointcloud.
            // extracted_pointcloud.HiddenPointRemoval(camera_position, 1.8);
            // extracted_pointcloud->SegmentPlane(0.01, 3, 100);
            // integration_shadow.Stop();
            auto pcd_legacy =
                std::make_shared<open3d::geometry::PointCloud>(extracted_pointcloud.ToLegacy());
            open3d::io::WritePointCloud("pcd_bilateral" + std::to_string(frame_id++) + ".ply",
                                        *pcd_legacy);
            // open3d::t::io::WritePointCloud("/home/oem/avena_system_clahe_test_ws/src/Open3dIntegration/pointclouds_with_settings/" + std::to_string(frame_id++) + ".ply", extracted_pointcloud);
            utility::LogInfo("Time: {}s, Frame {}",
                             static_cast<double>(rs1.GetTimestamp()) * 1e-6,
                             frame_id - 1);
        }
    } while (!flag_exit);

    rs1.StopCapture();

    return 0;
}
